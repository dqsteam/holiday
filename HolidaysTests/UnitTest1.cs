using System;
using Holidays;
using Xunit;

namespace HolidaysTests
{
    public class UnitTest1
    {
        [Fact]
        public void NotHoliday()
        {

            var dt = new DateTime();

            //Grants a new empty holidays collection
            Base.FlushHolidays();
            var rs = dt.IsHoliday();

            Assert.False(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));

        }

        [Fact]
        public void FixedHoliday()
        {

            var dt = new DateTime();
            var codeName = "Holiday Test1";

            Base.FlushHolidays();
            Base.AddFixedHoliday(dt.Day, dt.Month, codeName);

            var rs = dt.IsHoliday();

            Assert.True(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.Equal(rs.CodeName, codeName);

        }

        [Fact]
        public void Easter1()
        {
            var dt = new DateTime(0, 4, 12);

            var rs = dt.IsHoliday();

            Assert.True(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));
            Assert.Equal(rs.CodeName, HolidayResult.RelativeHoliday.Easter.ToString());
        }

        [Fact]
        public void Easter2()
        {
            var dt = new DateTime(2021, 4, 4);

            var rs = dt.IsHoliday();

            Assert.True(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));
            Assert.Equal(rs.CodeName, HolidayResult.RelativeHoliday.Easter.ToString());
        }

        [Fact]
        public void NotEaster()
        {
            var dt = new DateTime(2021, 4, 4).AddDays(1);

            var rs = dt.IsHoliday();

            Assert.False(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));
            Assert.Null(rs.CodeName); 
        }


        [Fact]
        public void GoodFriday1()
        {
            var dt = new DateTime(2020, 4, 10);

            var rs = dt.IsHoliday();

            Assert.True(rs.IsHoliday);
            Assert.Equal(rs.Date, dt);
            Assert.True(rs.Type.HasFlag(HolidayResult.HolidayType.Relative));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.None));
            Assert.False(rs.Type.HasFlag(HolidayResult.HolidayType.Fixed));
            Assert.Equal(rs.CodeName, HolidayResult.RelativeHoliday.GoodFriday.ToString());
        }
    }
}
