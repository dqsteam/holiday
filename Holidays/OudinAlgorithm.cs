﻿using System;
namespace Holidays
{
    public class OudinAlgorithm
    {
        /*
         * K_FACTOR related to 1700 
         */
        const int K_FACTOR_RELATIVE_CENTURY = 17;

        /*
         * Leap Year occurs each 4 years (366 days)
         */
        const int LEAP_YEAR_INTERVAL_IN_YEARS = 4;
        const int LEAP_YEAR_INTERVAL_IN_CENTURIES = 4;

        /*
         * Moon month is 29.5 earth days. 30 for calc
         */
        const int MOON_CYCLE_IN_DAYS = 30;
        const int FULL_MOON_DAYS_SINCE_NEW_MOON_IN_DAYS = MOON_CYCLE_IN_DAYS / 2;
        const int MARCH_EQUINOX_MONTH_NUMBER = 3;
        private const int METONIC_CYCLE_IN_YEARS = 19;
        private const int LEAP_YEARS_IN_A_CENTURY = 25;

        public OudinAlgorithm()
        {
        }

        public static DateTime GetEasterDateForYear(int year)
        {

            int century = year / 100;
            int centuryInfluencyInDays = century - (century / LEAP_YEAR_INTERVAL_IN_CENTURIES); //4th centuries leap years

            int nineteenYearsCyclePos = year % METONIC_CYCLE_IN_YEARS;

            int kFactor = (century - K_FACTOR_RELATIVE_CENTURY) / LEAP_YEARS_IN_A_CENTURY;
            int kFactorInfluenceInDays = ((century - kFactor) / 3);

            int newMoonDayLocatorM22 = centuryInfluencyInDays - kFactorInfluenceInDays + (nineteenYearsCyclePos * METONIC_CYCLE_IN_YEARS);
            int fullMoonDayLocatorM22 = newMoonDayLocatorM22 + FULL_MOON_DAYS_SINCE_NEW_MOON_IN_DAYS;
            int nearestNextFullMoonM22_Epact = fullMoonDayLocatorM22 % MOON_CYCLE_IN_DAYS;


            int nineteenYearsCycleLapseAdjust = 2 - century + (century / LEAP_YEAR_INTERVAL_IN_YEARS); //adiciona um dia a cada 400 anos / 4 séculos
            int leapYearDayAdjust = year + (year / LEAP_YEAR_INTERVAL_IN_YEARS); //adicionar um dia a cada 4 anos (bissexto)

            int weekDayLocatorM22 = leapYearDayAdjust + nearestNextFullMoonM22_Epact + nineteenYearsCycleLapseAdjust; //adiciona os ajustes calculados ao dia de referencia da lua cheia
            int weekDay = weekDayLocatorM22 % 7; //identifica o dia da semana que vai cair a lua cheia

            int monthThreshold = nearestNextFullMoonM22_Epact - weekDay; //encontra o limite para pascoa em março ou abril, < 4, março. >= 4, abril

            int easterMonth = MARCH_EQUINOX_MONTH_NUMBER + (monthThreshold + 40) / 44;

            int easterDay = monthThreshold + 28 - (31 * (easterMonth / 4));

            var easter = new DateTime(year, easterMonth, easterDay);

            return easter;
        }
    }
}
