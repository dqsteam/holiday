﻿using System;
namespace Holidays
{
    public class FixedHoliday
    {
        public FixedHoliday()
        {

        }

        public int Day { get; set; }
        public int Month { get; set; }
        public string CodeName { get; set; }
    }
}
