﻿using System;
using System.Collections.Generic;
using static Holidays.HolidayResult;

namespace Holidays
{
    public static class Base
    {
        public enum RelativeHolidayAlgorithm
        {
            Oudin_Default,
            Ng_hwaya_New
        }

        static List<FixedHoliday> Holidays {  get;  set; }

        static Base()
        {
            Holidays = new List<FixedHoliday>();  
        }

        public static HolidayResult IsHoliday(this DateTime dateTime, HolidayResult.HolidayType calcType = HolidayResult.HolidayType.Fixed | HolidayResult.HolidayType.Relative, RelativeHolidayAlgorithm algo = RelativeHolidayAlgorithm.Oudin_Default)
        {
            var hr = new HolidayResult(dateTime);

            if (calcType.HasFlag(HolidayResult.HolidayType.Fixed))
                GetFixedHolidays(hr);

            if (calcType.HasFlag(HolidayResult.HolidayType.Relative))
                GetRelativeHolidays(hr, algo);


            return hr;

        }

        public static void FlushHolidays()
        {
            Holidays.Clear();
        }

        private static void GetRelativeHolidays(HolidayResult hr, RelativeHolidayAlgorithm algo)
        {
            switch (algo)
            {
                case RelativeHolidayAlgorithm.Ng_hwaya_New:
                    CalcNg_hwaya_RelativeHoliday(hr);
                    break;
                case RelativeHolidayAlgorithm.Oudin_Default:
                default:
                    CalcOudin_RelativeHoliday(hr);
                    break;
            }
        }

        private static void CalcOudin_RelativeHoliday(HolidayResult hr)
        {
            var easter = OudinAlgorithm.GetEasterDateForYear(hr.Date.Year);
            GetRelativeHolidays(easter, hr);

        }

        private static void GetRelativeHolidays(DateTime easter, HolidayResult hr)
        {
            Dictionary<DateTime, RelativeHoliday> map = new Dictionary<DateTime, RelativeHoliday>();

            map.Add(easter, RelativeHoliday.Easter);
            map.Add(easter.AddDays(-46), RelativeHoliday.AshWednesday);
            map.Add(easter.AddDays(-47), RelativeHoliday.Carnival);
            map.Add(easter.AddDays(-2), RelativeHoliday.GoodFriday);
            map.Add(easter.AddDays(60), RelativeHoliday.CorpusChristi);


            var rh = map.GetValueOrDefault(hr.Date.Date, RelativeHoliday.None);

            if (rh != RelativeHoliday.None)
            {
                hr.SetRelativeHoliday(rh);
            }
        }

        private static void CalcNg_hwaya_RelativeHoliday(HolidayResult hr)
        {
            throw new NotImplementedException();
        }

        private static void GetFixedHolidays(HolidayResult hr)
        {
            var h = Holidays.Find(h => h.Day == hr.Date.Day && h.Month == hr.Date.Month);

            if (h != null) 
                hr.SetFixedHoliday(h);
        }


        public static void AddFixedHoliday(int day, int month, string codeName)
        {
            Holidays.Add(new FixedHoliday { Day = day, Month = month, CodeName = codeName });
        }
    }
}
