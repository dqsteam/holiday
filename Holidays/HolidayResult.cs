﻿using System;
namespace Holidays
{
    public class HolidayResult
    {

        [Flags]
        public enum HolidayType
        {
            Fixed = 1,
            Relative = 2,
            None = 4
        }

        public enum RelativeHoliday
        {
            None,
            Easter,
            Carnival,
            CorpusChristi,
            GoodFriday,
            AshWednesday,
        }

        public HolidayResult(DateTime dateTime)
        {
            this.Date = dateTime;
            this.Type = HolidayType.None;
        }


        public bool IsHoliday { get; private set; }
        public DateTime Date { get; private set; }
        public HolidayType Type { get; private set; }
        public string CodeName { get; private set; }

        public void SetFixedHoliday(FixedHoliday fixedHoliday)
        {
            if (fixedHoliday != null)
            {
                this.IsHoliday = true;
                this.CodeName = fixedHoliday.CodeName;

                if (this.Type == HolidayType.None)
                    this.Type = HolidayType.Fixed;
                else
                    this.Type |= HolidayType.Fixed;
            }
        }

        public void SetRelativeHoliday(RelativeHoliday relativeHoliday)
        {
            this.IsHoliday = true;
            this.CodeName = relativeHoliday.ToString();

            if (this.Type == HolidayType.None)
                this.Type = HolidayType.Relative;
            else
                this.Type |= HolidayType.Relative;
        }
    }
}
